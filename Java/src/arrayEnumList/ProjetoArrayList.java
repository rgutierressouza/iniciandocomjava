package arrayEnumList;

import java.util.ArrayList;

public class ProjetoArrayList {

	public static void main(String[] args) {
		String empresa1 = "Moda & Casa";
		String empresa2 = "Mc Donald's";
		String empresa3 = "Riachuelo";

		ArrayList<String> empresas = new ArrayList<>();
		
		empresas.add(empresa1);
		empresas.add(empresa2);
		empresas.add(empresa3);
		
		System.out.println(empresas);
		System.out.println(empresas.size());
		
		empresas.remove(2);
		
		System.out.println(empresas);
		System.out.println(empresas.size());
	}
}
