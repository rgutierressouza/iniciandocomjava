package arrayEnumList;

public class ProjetoEnum {

	public static void main(String[] args) {
		System.out.println("Dia da semana : " + diasSemana.segunda.getDiaSemana());
	}
	
	public enum diasSemana {
		
		domingo("Domingo"),
		segunda("segunda-feira"),
		terca("ter�a-feira"),
		quarta("quarta-feira"),
		quinta("quinta-feira"),
		sexta("sexta-feira"),
		sabado("s�bado");
		
		private String valor;
		
		diasSemana(String valor) {
			this.valor = valor;
		}
		
		public String getDiaSemana() {
			return this.valor;
		}
	}
}
