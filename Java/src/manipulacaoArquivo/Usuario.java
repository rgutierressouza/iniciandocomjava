package manipulacaoArquivo;

public class Usuario {

	private String usuario = "teste";
	private String senha = "12345";
	
	public Usuario() {}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
}
