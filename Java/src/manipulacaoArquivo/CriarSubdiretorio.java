package manipulacaoArquivo;

import java.io.File;

public class CriarSubdiretorio {

	public static void main(String[] args) {
		String novoDiretorio = "novoDiretorio";

		File diretorio = new File("C:\\Users\\r.gsouza\\Documents\\Estudo\\" + novoDiretorio);

		if (diretorio.exists()) {
			System.out.println("Diret�rio existente");
		} else {
			//Comando para criar um novo diret�rio
			diretorio.mkdir();
			System.out.println("Diretorio criado em : " + diretorio.getAbsolutePath());
		}
		String novoSubdiretorio = "subdiretorioCriado";
		File subdiretorio = new File(diretorio.getAbsolutePath() + "\\" + novoSubdiretorio);
		
		if(subdiretorio.exists()) {
			System.out.println("Subdiretorio existente");
		}else {
		subdiretorio.mkdir();
		System.out.println("\nCria��o realizada com sucesso!");
		System.out.println("\nDiret�rio: " + diretorio.getName() + "\nSubdiretorio : " + subdiretorio.getName() 
		+  "\nLocal da pasta :  "+ subdiretorio.getAbsolutePath());
		}
	}

}
