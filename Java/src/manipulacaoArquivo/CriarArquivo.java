package manipulacaoArquivo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class CriarArquivo {
	String path = "C:\\Users\\r.gsouza\\Documents\\Estudo\\";
	Scanner sc = new Scanner(System.in);
	File diretorio;
	File subdiretorio;

	public void criarDiretorio() {

		System.out.println("Informe o nome do diret�rio : ");

		String novoDiretorio = sc.next();

		diretorio = new File(path + novoDiretorio);

		if (this.diretorio.exists()) {
			System.out.println("Diret�rio existente");
		} else {
			// Comando para criar um novo diret�rio
			diretorio.mkdir();
			System.out.println("Diretorio criado em : " + diretorio.getAbsolutePath());
		}
	}

	public void criarSubdiretorio() {
		System.out.println("Informe o nome do Subdiret�rio : ");

		String novoSubdiretorio = sc.next();
		System.out.println(diretorio.getAbsolutePath());

		subdiretorio = new File(diretorio.getAbsolutePath() + "\\" + novoSubdiretorio);

		if (subdiretorio.exists()) {
			System.out.println("Subdiretorio existente");
		} else {
			subdiretorio.mkdir();
			System.out.println("\nCria��o realizada com sucesso!");
			System.out.println("\nDiret�rio: " + diretorio.getName() + "\nSubdiretorio : " + subdiretorio.getName()
					+ "\nLocal da pasta :  " + subdiretorio.getAbsolutePath());
		}
	}

	public void criarArquivo() {
		System.out.println("Informe o nome do arquivo : ");
		String nomeArquivo = sc.next();
		try {
			// Criar arquivo
			FileWriter arquivoTxt = new FileWriter(subdiretorio.getAbsolutePath() + "\\" + nomeArquivo + ".txt");
			// Escrever
			PrintWriter escreverArq = new PrintWriter(arquivoTxt);

			// M�todo para escrever
			escreverArq.println("Cabe�alho do arquivo TXT");
			escreverArq.println("TESTE DE ESCRITA NO ARQUIVO");

			// Fechar o arquivo
			arquivoTxt.close();
			System.out.println("Arquivo criado com sucesso!!! ");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {

		CriarArquivo ca = new CriarArquivo();

		ca.criarDiretorio();

		ca.criarSubdiretorio();

		ca.criarArquivo();
	}

}
