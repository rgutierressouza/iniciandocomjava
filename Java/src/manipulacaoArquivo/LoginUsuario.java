package manipulacaoArquivo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class LoginUsuario {
	Usuario userLogin = new Usuario();
	Calendar calendar = Calendar.getInstance();

	File diretorio;
	File subdiretorio;
	String usuario;
	String senha;
	String path = "C:\\Users\\r.gsouza\\Documents\\Estudo\\Login";
	Scanner sc = new Scanner(System.in);

	public static void main(String args[]) {
		LoginUsuario login = new LoginUsuario();
		login.controleLoginArquivo();
	}

	public void controleLoginArquivo() {

		System.out.println("Informe o login:");
		usuario = sc.nextLine();
		System.out.println("Informe a senha:");
		senha = sc.nextLine();

		if (usuario.equals(userLogin.getUsuario()) && senha.equals(userLogin.getSenha())) {
			LoginUsuario login = new LoginUsuario();
			login.criarArquivo();
			System.out.println("Login efetuado com sucesso");
		} else {
			System.out.println("Usu�rio ou senha inv�lidos");
		}
	}

	public void criarArquivo() {
		try {

			File arquivoDir = new File(path + "\\arquivologin.txt");

			if (arquivoDir.isFile()) {
				// Criar arquivo
				// O True possibilita que seja gerado um novo registro a cada execu��o.
				FileWriter arquivoTxt = new FileWriter(path + "\\arquivologin.txt", true);
				// Escrever
				PrintWriter escreverArq = new PrintWriter(arquivoTxt);
				// M�todo para escrever
				escreverArq.println("----------------------------------------");
				escreverArq.printf("Usuarios e senha");
				escreverArq.println("");
				escreverArq.printf("Login efetuado em : " + geraData());
				escreverArq.printf("\nUsuario: " + userLogin.getUsuario());
				escreverArq.printf("\nSenha: " + userLogin.getSenha());
				escreverArq.println("");
				// Fechar o arquivo
				arquivoTxt.close();

			} else {
				// Criar arquivo
				// O True possibilita que seja gerado um novo registro a cada execu��o.
				FileWriter arquivoTxt = new FileWriter(path + "\\arquivologin.txt", true);
				// Escrever
				PrintWriter escreverArq = new PrintWriter(arquivoTxt);

				// M�todo para escrever
				escreverArq.println("----------------------------------------");
				escreverArq.printf("Usuarios e senha");
				escreverArq.println("");
				escreverArq.printf("Login efetuado em : " + geraData());
				escreverArq.printf("\nUsuario: " + userLogin.getUsuario());
				escreverArq.printf("\nSenha: " + userLogin.getSenha());
				escreverArq.println("");
				// Fechar o arquivo
				arquivoTxt.close();
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		sc.close();
	}

	public String geraData() {
		Date d = calendar.getTime();
		DateFormat dataHora = DateFormat.getDateTimeInstance();
		return dataHora.format(d);
	}
}
