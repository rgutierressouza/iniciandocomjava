package manipulacaoArquivo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LerArquivo {

	public static void main(String args[]) {
		LerArquivo ler = new LerArquivo();
		ler.lerArquivo();
	}

	public void lerArquivo() {
		try {
			// Realizar leitura do arquivo
			FileReader arquivo = new FileReader("C:\\Users\\r.gsouza\\Documents\\Estudo\\Login\\arquivologin.txt");
			BufferedReader leituraArquivo = new BufferedReader(arquivo);

			String linha = leituraArquivo.readLine();

			while (linha != null) {
				System.out.printf("%s \n", linha);
				linha = leituraArquivo.readLine(); // Atribuição para deixar variavel diferente de null e dar
													// continuidade ao funcionamento do laço while
			}

			leituraArquivo.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}
}
