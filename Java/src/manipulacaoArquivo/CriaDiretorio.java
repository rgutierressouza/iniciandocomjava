package manipulacaoArquivo;

import java.io.File;

public class CriaDiretorio {

	public static void main(String[] args) {
		String novoDiretorio = "novoDiretorio";

		File diretorio = new File("C:\\Users\\r.gsouza\\Documents\\Estudo\\" + novoDiretorio);

		if (diretorio.exists()) {
			System.out.println("Diretório existente");
		} else {
			//Comando para criar um novo diretório
			diretorio.mkdir();
			System.out.println("Diretorio criado em : " + diretorio.getAbsolutePath());
		}
	}
}
