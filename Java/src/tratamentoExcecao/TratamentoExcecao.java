package tratamentoExcecao;

/*Tratamento de exce��es e erros
 * try {
 * 
 * local onde � digitado o c�digo que pode ocorrer um erro
 * 
 * }
 * 
 * O try verifica o c�digo e se sua execu��o est� correta. Se n�o ocorrer nenhum erro, a execu��o do programa seguir� normalmente,
 * caso ao contr�rio a execu��o se direcionar� para o catch tratar o erro  
 * 
 * catch(ClasseDeExce��o inst�nciaDaExce��o ){
 * 
 * Escrito o tratamento para a exce��o
 * 
 * toString(); -> Converte o dados da exce��o em string
 * printStackTrace(); -> Imprime no console, utilizado mais para o programador testar
 * getCause(); -> Retorna a causa da exce��o
 * getMessage(); -> Retorna uma string com o erro
 * Finally(); Executa o c�digo mesmo quando a exce��o � lan�ada
 * }
 */

public class TratamentoExcecao {

	public static void main(String[] args) {
	
	}
}
