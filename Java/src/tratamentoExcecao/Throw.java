package tratamentoExcecao;

import java.util.EmptyStackException;
import java.util.Scanner;

public class Throw {

	public static void main(String[] args) {
		saque();
	}

	public static void saque() {

		Scanner sc = new Scanner(System.in);

		int saque = 0;

		System.out.println("Informe o valor para saque:");
		saque = sc.nextInt();
		if (saque == 0) {
			extracted(null);
		}
		if (saque > 400) {
			IllegalArgumentException error = new IllegalArgumentException();
			extracted(error);
		} else {
			System.out.println("Saque realizado com sucesso! \n valor sacado: R$ " + saque);
		}
		sc.close();
	}

	// M�todo utilizado para lan�ar as exce��es em seus casos
	private static void extracted(IllegalArgumentException error) {
		if (error == null) {
			throw new EmptyStackException();
		} else {
			throw error;
		}
	}
}
