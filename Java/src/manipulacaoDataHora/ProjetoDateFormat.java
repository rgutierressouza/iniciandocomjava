package manipulacaoDataHora;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class ProjetoDateFormat {
	public static void main (String args[]) {
		Calendar c = Calendar.getInstance();
		
		Date d = c.getTime();
		
		System.out.println(d);
		
		//Formatando a data
		DateFormat date = DateFormat.getDateInstance();
		System.out.println(date.format(d));
		
		//Formatando a hora
		DateFormat hora = DateFormat.getTimeInstance();
		System.out.println(hora.format(d));
		
		//Formatar data e Hora
		DateFormat dataHora = DateFormat.getDateTimeInstance();
		System.out.println(dataHora.format(d));
	}
}
