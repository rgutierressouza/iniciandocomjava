package manipulacaoDataHora;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;

public class ProjetoTime {

	public static void main(String[] args) {
		Time time = Time.valueOf(LocalTime.now());
		System.out.println(time);
		
		Year year = Year.now();
		System.out.println(year);
		
		YearMonth ym = YearMonth.now();
		System.out.println(ym);
		
		MonthDay md = MonthDay.now();
		System.out.println(md);
		
		LocalDateTime ldt = LocalDateTime.now();
		System.out.println(ldt);
		
		OffsetDateTime osdt = OffsetDateTime.now();
		System.out.println(osdt);
		
		ZoneId zona = ZoneId.systemDefault();
		System.out.println(zona);
		
		Period period = Period.ofYears(2018);//Ano segundo a ISO-8601
		System.out.println(period);
	}

}
