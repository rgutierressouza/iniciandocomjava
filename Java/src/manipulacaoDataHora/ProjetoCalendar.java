/*
 * Data: 26/09/2018
 */
package manipulacaoDataHora;

import java.util.Calendar;

public class ProjetoCalendar {

	public static void main(String[] args) {
		Calendar c = Calendar.getInstance();
		System.out.println(c.getTime()); // Passa a hora e data atual
		System.out.println(c.get(Calendar.YEAR));// Retorna o Ano
		System.out.println(c.get(Calendar.MONTH));// Retorna o m�s
		System.out.println(c.get(Calendar.DAY_OF_YEAR));// Retorna dia do ano, contagem dos 365 dias
		System.out.println(c.get(Calendar.DAY_OF_WEEK));// Retorna dia da semana, retorna o dia contando a partir de Domingo.
		//Mas a contagem � feita baseada na semana ter 7 dias e domingo ser o dia 0.
		//Ex: Este c�digo foi escrito numa quarta-feira, neste caso ser� exibido 4, lembrando que domingo � 0
		System.out.println(c.get(Calendar.DAY_OF_WEEK_IN_MONTH));//Retorna o dia da semana no m�s, m�s ideia de anteriormente.
		System.out.println(c.get(Calendar.DATE)); //Retorna o dia atual
	}
}
