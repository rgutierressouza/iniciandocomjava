package manipulacaoDataHora;

import java.sql.Date;
import java.time.LocalDate;

public class ProjetoDate {

	public static void main (String args[]) {
		Date date = Date.valueOf(LocalDate.now());
		System.out.println(date);
		Date a = Date.valueOf(LocalDate.MAX);
		System.out.println(a);
		Date b = Date.valueOf(LocalDate.MIN);
		System.out.println(b);
	}
}
