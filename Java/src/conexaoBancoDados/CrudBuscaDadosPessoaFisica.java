package conexaoBancoDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CrudBuscaDadosPessoaFisica {

	private Connection conectar() {
		// url de conex�o
		String url = "jdbc:sqlite:bd_mimos_kr/mimoskrbd.db";
		// Variavel de conex�o
		Connection conn = null;

		try {

			conn = DriverManager.getConnection(url);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return conn;
	}

	public void consultarDados() {

		String sql = "SELECT id_pessoa,nome,sobrenome,cpf FROM PESSOA";

		try {
			Connection conn = this.conectar();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			System.out.println("Dados Consultados na base");
			while(rs.next()) {
			System.out.println(rs.getInt("id_pessoa")+ " "
					+ rs.getString("nome") + " " 
					+ rs.getString("sobrenome") + " " 
					+ rs.getString("cpf"));
			}
		} catch (SQLException e) {
			System.out.println("Erro ao consultar a base de dados! \n Erro: " + e.getMessage());
		}
	}
}
