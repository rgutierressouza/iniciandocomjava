package conexaoBancoDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CrudDeletarDadosPessoaFisica {
	
	
	private Connection conectar() {
		// url de conex�o
		String url = "jdbc:sqlite:bd_mimos_kr/mimoskrbd.db";
		// Variavel de conex�o
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}
	
	public void deletar(int id) {
		String sql = "DELETE FROM pessoa WHERE id_pessoa = ?";
	   try {
		Connection conn = this.conectar();
		
		   PreparedStatement pstmt = conn.prepareStatement(sql);
		   pstmt.setInt(1, id);
		   
		   pstmt.executeUpdate();
		   
	} catch (SQLException e) {
		System.out.println(e.getMessage());
	}
	}
}
