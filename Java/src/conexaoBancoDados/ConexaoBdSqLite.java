package conexaoBancoDados;

public class ConexaoBdSqLite {

	public static void main(String[] args) throws ClassNotFoundException {
		Conexao conexao = new Conexao();
		CriarTabelas newtab = new CriarTabelas();
		CrudInserirPessoaFisica newPessoa = new CrudInserirPessoaFisica();
		CrudBuscaDadosPessoaFisica busca = new CrudBuscaDadosPessoaFisica();
		CrudAtualizaDadosPessoaFisica atualizar = new CrudAtualizaDadosPessoaFisica();
		CrudDeletarDadosPessoaFisica deletar = new CrudDeletarDadosPessoaFisica();
		
		conexao.conectar();
		newtab.criarTabela();

		newPessoa.inserir("Roberto", "Souza", "00913288020");
		atualizar.atualizarDados(4, "Luciano", "Silveira", "123456789");
		busca.consultarDados();
		deletar.deletar(2);
		deletar.deletar(4);
		deletar.deletar(6);
		busca.consultarDados();
		conexao.desconectar();
	}
}
