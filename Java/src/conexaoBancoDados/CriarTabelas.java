package conexaoBancoDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CriarTabelas {

	// M�todo para cria��o das tabelas
	public void criarTabela() {
		// url de conex�o
		String url = "jdbc:sqlite:bd_mimos_kr/mimoskrbd.db";

		// Sql Statement
		String sql = "CREATE TABLE IF NOT EXISTS pessoa ("
				+ "\n" + "id_pessoa integer primary key not null, \n"
				+ "nome text NOT NULL, \n"
				+ "sobrenome text NOT NULL,"
				+ "dt_nasc text , \n"
				+ "sexo TEXT , \n"
				+ "estadocivil text , \n"
				+ "regimeBens text, \n"
				+ "rg text, \n"
				+ "cpf text NOT NULL, \n"
				+ "naturalidade TEXT, \n"
				+ "profissao TEXT \n" + ");";

		System.out.println("Tabela criada com sucesso!");
		try {
			Connection conn = DriverManager.getConnection(url);

			// instanciar o Statement
			Statement stmt = conn.createStatement();
			// Criar nova tabela
			stmt.executeQuery(sql);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
