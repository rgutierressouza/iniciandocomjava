package conexaoBancoDados;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CrudInserirPessoaFisica {

	private Connection conectar() {
		// url de conexão
		String url = "jdbc:sqlite:bd_mimos_kr/mimoskrbd.db";
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(url);
			
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}
	
	
	public void inserir( String nome,String sobrenome, String cpf) {
		String sql = "INSERT INTO PESSOA (nome,sobrenome,cpf) VALUES (?,?,?)";
		
		try {
			Connection conn = this.conectar();
			
			PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, nome);
				pstmt.setString(2, sobrenome);
				pstmt.setString(3, cpf);
				
				pstmt.executeUpdate();
			
				System.out.println("Pessoa inserida com sucesso");
				
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
