package conexaoBancoDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CrudAtualizaDadosPessoaFisica {

	private Connection conectar() {
		// url de conex�o
		String url = "jdbc:sqlite:bd_mimos_kr/mimoskrbd.db";
		// Variavel de conex�o
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}

	/*
	 * M�todo para atualizar dados do usu�rio
	 */
	public void atualizarDados(int id, String nome,String sobrenome,String cpf) {
		try {
			Connection conn = this.conectar();
			
			String sql = "UPDATE PESSOA SET nome = ?, sobrenome = ?, cpf = ? where id_pessoa = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, nome );
			pstmt.setString(2, sobrenome );
			pstmt.setString(3, cpf);
			pstmt.setInt(4, id);
			
			pstmt.executeUpdate();
			
			System.out.println("Dados atualizados do usu�rio com sucesso!!");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
	}
}
