package conexaoBancoDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * 
 * @author Roberto
 * 
 */
public class Conexao {

	// Atributo de conex�o com o banco de dados
	private Connection conexao; // declara��o da variavel conex�o
	/*
	 * M�todo que conecta ao banco de dados e caso n�o exista cria um novo banco
	 * 
	 * @return true - Conex�o realizada com sucesso
	 */
	public boolean conectar() throws ClassNotFoundException {
		try {
			// Comando para abrir a biblioteca do SqLite
			Class.forName("org.sqlite.JDBC");

			// url de conex�o
			String url = "jdbc:sqlite:bd_mimos_kr/mimoskrbd.db";

			// Conectar com o banco
			this.conexao = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		System.out.println("Conex�o realizada com sucesso!!");
		return true;
	}	
	/*
	 * M�todo para desconectar no banco de dados
	 * @return true - Quando desconectado
	 */
	public boolean desconectar() {
		try {
			if(this.conexao.isClosed() == false)
				this.conexao.close();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		System.out.println("Desconectou com sucesso!!");
		return true;
	}
}
