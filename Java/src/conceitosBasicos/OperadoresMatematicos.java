package conceitosBasicos;//Pacote que pertence a classe

public class OperadoresMatematicos {// Inicia a classe

	public static void main(String args[]) {// Inicio do m�todo main

		// Principais operadores matematicos +(Adi��o), -(Subtra��o), *(Multiplica��o) e /(Divis�o) 
		
		//Opera��o de soma
		int soma = 7 + 6;// Resultado: 13
		System.out.println("Resultado da soma : " + soma);// Aqui imprimimos o resultado da soma
		
		int subtracao = 29 - 7;// Resultado: 22
		System.out.println("Resultado da subtra��o : " + subtracao);// Aqui imprimimos o resultado da subtra��o
		
		int multiplicacao = 7 * 7 ;// Resultado: 49
		System.out.println("Resultado da multiplica��o : " + multiplicacao);// Aqui imprimimos o resultado da multiplica��o
		
		int divisao = 50 / 5;// Resultado: 10
		System.out.println("Resultado da multiplica��o : " + divisao);// Aqui imprimimos o resultado da divis�o
		
		
		// Resto de uma divis�o
		int resto = 5 % 3;// Resultado: 2
		System.out.println("Resultado da multiplica��o : " + resto);// Aqui imprimimos o resultado da divis�o
		
		
	}// Fim do m�todo main
}// Fim da classe
