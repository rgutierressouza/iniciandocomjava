package conceitosBasicos;//Pacote ao qual pertence a classe

public class TiposDeVariaveis {//Inicio da classe

	public static void main(String args[]) {//Inicio do m�todo main
		//Decla��o de variaveis e seus tipos
		
		//tipos primitivos
		char variavelCaracter = 'a';
		short variavelNumerico = 179;
		int variavelNumericoInteiro = 4567788;
		byte variavelByte = 0100;
		long variavelNumericoLong = 909320099;
		
		//Ponto Flutuante
		double variravelDouble = 6.9;
		float variavelFloat = 698.8f;
		
		//Booleano
		boolean variavelBooleanaVerdadeira = true;
		boolean variavelBooleanaFalsa = false;
		
		String variavelString = "String � um objeto onde possibilita a concatena��o de varios char,\n"
				+ "possibilitando que seja escrita esta frase";
		
		System.out.println("_________Tipos Primitivos__________\n");
		System.out.println( " Tipo Primitivo : char " + variavelCaracter);
		System.out.println( " Tipo Primitivo : short " + variavelNumerico);
		System.out.println( " Tipo Primitivo : byte " + variavelByte);
		System.out.println( " Tipo Primitivo : int " + variavelNumericoInteiro);
		System.out.println( " Tipo Primitivo : long " + variavelNumericoLong);
		System.out.println("___________________________________");
		System.out.println("_________Tipos Flutuantes__________\n");
		System.out.println( " Ponto Flutuante : double " + variravelDouble);
		System.out.println( " Ponto Flutuante : float " + variavelFloat);
		System.out.println("___________________________________");
		System.out.println("___________Tipo Booleano___________\n");		
		System.out.println( " Tipo Booleano: boolean(false) " + variavelBooleanaFalsa);
		System.out.println( " Tipo Booleano: boolean(true) " + variavelBooleanaVerdadeira);
		System.out.println("*************************************");
		System.out.println("___________Classe String___________\n");		
		System.out.println( " Classe String " + variavelString);
	}//Fim do m�todo main
}//Fim da classe
