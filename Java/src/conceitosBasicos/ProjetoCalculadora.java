package conceitosBasicos; // Pertence ao pacote

import java.util.Scanner;

public class ProjetoCalculadora {// Inicio da classe

	public static void main(String[] args) {// Inicio do metodo main 
		
		
		int resultado = 0;
		int operacao = 0;
		String operacaoTipo = "";
		int a = 0,b = 0;
		Scanner sc = new Scanner(System.in);// Inst�ncia do Scanner
		
//		System.out.println("___________Calculadora_________");
//		System.out.println(" Digite o primeiro valor: ");
//		a = sc.nextInt();// Aguarda o primeiro valor ser digitado pelo usu�rio
//		System.out.println(" Digite o segundo valor: ");
//		b = sc.nextInt();// Aguarda o segundo valor ser digitado pelo usu�rio
//		
//		System.out.println(" -----------------------------------");
//		System.out.println(" Selecione uma opera��o: ");
//		System.out.println(" 1 - Soma \n 2 - Subtra��o \n 3 - Multiplica��o \n 4 - Divis�o \n 5 - Sair ");
//		operacao = sc.nextInt();// Aguarda o que o usu�rio selecione a opera��o que ser� feita
//		System.out.println("A " + operacaoTipo +  " de " + a  + " com " + b + " � igual a :  " + resultado);
//		
		while (operacao != 5) {// Inicio do la�o de repeti��o - Enquanto n�o for selecionado 5, a execu��o continua
			System.out.println("___________Calculadora_________");
			System.out.println(" Digite o primeiro valor: ");
			a = sc.nextInt();// Aguarda o primeiro valor ser digitado pelo usu�rio
			System.out.println(" Digite o segundo valor: ");
			b = sc.nextInt();// Aguarda o segundo valor ser digitado pelo usu�rio
			
			System.out.println(" -----------------------------------");
			System.out.println(" Selecione uma opera��o: ");
			System.out.println(" 1 - Soma \n 2 - Subtra��o \n 3 - Multiplica��o \n 4 - Divis�o \n 5 - Sair ");
			operacao = sc.nextInt();
			
			switch (operacao) {// inicio do switch
			case 1:// Comportamento no caso de digitar 1
				operacaoTipo = "Soma";
				resultado = a + b; // � realizado a soma entre os dois numeros, atribuindo a soma � vari�vel resultado 
				System.out.println("A " + operacaoTipo +  " de " + a  + " com " + b + " � igual a :  " + resultado);
				break;
			case 2:// Comportamento no caso de digitar 2
				operacaoTipo = "Divis�o";
				resultado = a - b;// � realizado a subtra��o entre os dois numeros, atribuindo a soma � vari�vel resultado
				System.out.println("A " + operacaoTipo +  " de " + a  + " com " + b + " � igual a :  " + resultado);
				break;
			case 3:// Comportamento no caso de digitar 3
				operacaoTipo = "Multipica��o";
				resultado = a * b;// � realizado a multiplica��o entre os dois numeros, atribuindo a soma � vari�vel resultado
				System.out.println("A " + operacaoTipo +  " de " + a  + " com " + b + " � igual a :  " + resultado);
				break;
			case 4:// Comportamento no caso de digitar 4
				operacaoTipo = "Divis�o";
				resultado = a / b;// � realizado a divis�o entre os dois numeros, atribuindo a soma � vari�vel resultado
				System.out.println("A " + operacaoTipo +  " de " + a  + " com " + b + " � igual a :  " + resultado);
				break;
			case 5:// Comportamento no caso de digitar 5
				System.out.println("Aplica��o encerrada.");// Encerra a execu��o do programa imprimindo a mensagem "Aplica��o encerrada."
				break;
			default:
				break;
			}//Fim do switch
		}//Fim do la�o de repeti��o	
		sc.close();
	}//Fim do m�todo main
}// Fim da classe
