package conceitosBasicos;

public class LacoRepeticao {

	public static void main(String[] args) {
	
		int a = 0;
		
		while ( a <= 10 ) {
			System.out.println("La�o while : " + a );
			a++;
		} 
		
		System.out.println("----------------------------");
		
		for (int i =0; i <= 10;i++) {
			a = i;
			System.out.println("La�o for : " + a);
		}
	}
}
