package conceitosBasicos;// Classe pertence ao pacote conceitosBasicos

import java.util.Scanner;

public class ImprimindoEntradaDoTeclado {// Inicio da classe

	public static void main(String[] args) {// Inicio do m�todo

		Scanner sc = new Scanner(System.in);// Inst�ncia do Scanner
		System.out.println(" Digite uma mensagem: ");
		String mensagem = sc.nextLine();// Aguarda o texto digitado pelo usu�rio
		System.out.println("Voc� digitou a mensagem : " + mensagem);//imprime o texto escrito pelo usu�rio
		sc.close();//Finaliza a inst�ncia do Scanner
	}// Fim do m�todo
}// Fim da classe
