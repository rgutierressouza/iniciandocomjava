package tratamentoDados;

public class Strings {

	public static void main(String[] args) {
	char[] charString = {'r','o','b','e','r','t','o'};
	System.out.println(charString);
	
	String string = new String(charString);
	System.out.println(string);
	
	//Substituição de parte da String 
	String strings = "www.google.com";
	System.out.println(strings.replace("google", "yahoo"));
	
	String sub = "www.google.com";
	System.out.println(sub.substring(4, 10));
	}
}
