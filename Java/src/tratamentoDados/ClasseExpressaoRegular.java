package tratamentoDados;

import java.util.Scanner;

/*
 * \d -> Representa qualquer digito
 * /D -> Representa tudo menos digitos
 * \w -> Representa qualquer caracter de palavra
 * /W -> Representa tudo que não seja caracter na palavra
 * \s -> Representa qualquer espaço em branco
 * /S -> Representa tudo que não seja espaço em branco
 * [a-z] -> Representa qualquer letra minúsculo do alfabeto
 * [a-z] -> Representa qualquer letra maiúsculo do alfabeto
 * [a-zA-z] -> Representa qualquer letra do alfabeto minúsculo e maiúsculo
 * -----Expressões------
 * | -> Representa o ou 
 * . -> Substitui qualquer caracter
 * * -> O caractere anterior aparece nenhuma ou mais vezes
 * + -> Representa o caracter anterior aparece uma ou mais vezes
 * {a}-> O caracter anterior se repete 'a' vezes
 * {a,} -> o caracter anterior se repete pelo menos as vezes
 * {a,b} -> O caractere anterior se repete entre 'a' e 'b' vezes
 * Expressõo regular de CEP -> \\d(5)-\\d(3)
 */


public class ClasseExpressaoRegular {
	
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("CEP:");
		String cep = scanner.nextLine();
		if (cep.matches("\\d{5}-\\d{3}")) {
			System.out.println("Cep valido");
		} else {
			System.out.println("Cep inv�lido");
		}
		scanner.close();
	}
}
